/**
 * 
 */
package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author tritm
 *
 */
public class VendingMachineTest {
	/** Declaring necessary test objects for {@link VendingMachine} {@link VendingMachineItem}*/
	VendingMachine vend;
	VendingMachineItem itemA, itemB, itemC, itemD;

	/**
	 * Initializes the necessary test objects for the test cases to use.
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		vend = new VendingMachine();
		itemA = new VendingMachineItem("apple", 1.5);
		itemB = new VendingMachineItem("bread", 2.0);
		itemC = new VendingMachineItem("candy", 2.5);
		itemD = new VendingMachineItem("drink", 3.0);
		vend.addItem(itemA, "A");
		vend.addItem(itemB, "B");
		vend.addItem(itemC, "C");
		vend.addItem(itemD, "D");
		vend.insertMoney(10.0);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		vend = null;
	}

	/**
	 * Because Precondition: The slot specified by the code must be empty.
	 * 		   Postcondition: The item is now at the slot specified by the code.
	 * So we can verify each item with each code is correct or not.
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#addItem(edu.towson.cis.cosc442.project2.vendingmachine.VendingMachineItem, java.lang.String)}.
	 */
	@Test
	public void testAddItem() {
		assertEquals(itemA, vend.getItem("A"));
		assertEquals(itemB, vend.getItem("B"));
		assertEquals(itemC, vend.getItem("C"));
		assertEquals(itemD, vend.getItem("D"));
	}

	/**
	 * Because Postcondition: If the code slot is not empty, the item in that slot is removed.
	 * We can test this method can remove item by code or not
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#removeItem(java.lang.String)}.
	 */
	@Test
	public void testRemoveItem() {
		assertEquals(itemA,vend.removeItem("A"));
		assertEquals(itemB,vend.removeItem("B"));
		assertEquals(itemC,vend.removeItem("C"));
		assertEquals(itemD,vend.removeItem("D"));
	}

	/**
	 * Because Postcondition: balance is now the previous balance + amount.
	 * So we can test if balance = amount insert => method is correct.
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#insertMoney(double)}.
	 */
	@Test
	public void testInsertMoney() {
		assertEquals(10.0, vend.getBalance(), 0.001);
	}

	/**
	 * Because Postcondition: the balance is >= 0 and remains the same as it was before the function was called.
	 * So we can test method getBalance return same amount => method is correct.
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#getBalance()}.
	 */
	@Test
	public void testGetBalance() {
		assertEquals(10.0, vend.getBalance(), 0.001);
	}

	/**
	 * Because Postcondition: The amount of the item is subtracted from the balance.
	 * So we can test purchase item is correct or not, and the balance is correct or not.
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#makePurchase(java.lang.String)}.
	 */
	@Test
	public void testMakePurchase() {
		assertTrue(vend.makePurchase("A"));
		assertEquals(8.5, vend.getBalance(), 0.001);
		assertTrue(vend.makePurchase("B"));
		assertEquals(6.5, vend.getBalance(), 0.001);
		assertTrue(vend.makePurchase("C"));
		assertEquals(4.0, vend.getBalance(), 0.001);
		assertTrue(vend.makePurchase("D"));
		assertEquals(1.0, vend.getBalance(), 0.001);
	}

	/**
	 * Because Returns the amount of change in the machine and sets the balance to 0.
	 * So we can test the change return is correct or not, and the balance is 0 or not.
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachine#returnChange()}.
	 */
	@Test
	public void testReturnChange() {
		vend.makePurchase("A");
		vend.makePurchase("B");
		vend.makePurchase("C");
		vend.makePurchase("D");
		assertEquals(1.0, vend.returnChange(), 0.001); //1.0 is the change
		assertEquals(0.0, vend.getBalance(), 0.001);
	}

}
