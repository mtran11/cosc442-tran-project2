/**
 * 
 */
package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author tritm
 *
 */
public class VendingMachineItemTest {
	/** Declaring necessary test objects for {@link VendingMachine} {@link VendingMachineItem}*/
	VendingMachine vend;
	VendingMachineException reason;
	VendingMachineItem itemA, itemB, itemC, itemD, itemF;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		vend = new VendingMachine();
		reason = new VendingMachineException("Price cannot be less than zero");
		itemA = new VendingMachineItem("apple", 1.5);
		itemB = new VendingMachineItem("bread", 2.0);
		itemC = new VendingMachineItem("candy", 2.5);
		itemD = new VendingMachineItem("drink", 3.0);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		vend = null;
		reason = null;
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachineItem#VendingMachineItem(java.lang.String, double)}.
	 */
	@Test
	public void testVendingMachineItem() {
		assertSame("apple", itemA.getName());
		assertEquals(1.5, itemA.getPrice(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachineItem#getName()}.
	 */
	@Test
	public void testGetName() {
		assertSame("apple", itemA.getName());
		assertSame("bread", itemB.getName());
		assertSame("candy", itemC.getName());
		assertSame("drink", itemD.getName());
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project2.vendingmachine.VendingMachineItem#getPrice()}.
	 */
	@Test
	public void testGetPrice() {
		assertEquals(1.5, itemA.getPrice(), 0.001);
		assertEquals(2.0, itemB.getPrice(), 0.001);
		assertEquals(2.5, itemC.getPrice(), 0.001);
		assertEquals(3.0, itemD.getPrice(), 0.001);
	}

}
